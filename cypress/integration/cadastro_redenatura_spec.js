describe ( 'Cadastro Rede Natura', () => {
    it('focueses input on load', () => {
        cy.visit('https://cadastro.rede.natura.net/cadastro')

        function dateNow() {
            return Date.now();
          }

        var dateN = dateNow();
        
        cy.wait(3000)

        cy.screenshot('Formulario'+dateN+'')

        cy.get('#virtualAddress').type('testecypress'+dateN+'').should('have.value', 'testecypress'+dateN+'')
        cy.get('.verificar').click()
        cy.get('.reservar').click()   

        cy.get('#document1').type(''+cpf()+'')
        cy.get('#birthday').type('18021991').should('have.value', '18021991')
        cy.get('#name').type('Wagner', { delay: 100 }).should('have.value', 'Wagner')
        cy.get('#lastname').type('Savoia', { delay: 100 }).should('have.value', 'Savoia')
        cy.get('#email').type('testecypress'+dateN+'@teste.com').should('have.value', 'testecypress'+dateN+'@teste.com')
        cy.get('#confirmEmail').type('testecypress'+dateN+'@teste.com').should('have.value', 'testecypress'+dateN+'@teste.com')
        cy.get('#password').type('a1a2a3')
        cy.get('#confirmPassword').type('a1a2a3')
        cy.get('#phone').type('1932323232').should('have.value', '1932323232')
        cy.get('#cellphone').type('19993582304').should('have.value', '19993582304')
        cy.get('#dropdownGenero').select('Masculino')
        cy.get('#rg').type('478749888').should('have.value', '478749888')

        cy.get('#cep').type('13056487').should('have.value', '13056487')
        cy.get('#addressNumber').type('999').should('have.value', '999')
        cy.get('#addressComplement').type('Nenhum').should('have.value', 'Nenhum')

        cy.get('#contratoFranquia').check({ force: true }).should('be.checked')
        cy.get('#termosUso').check({ force: true }).should('be.checked')
        cy.get('#brokerAccepted').check({ force: true }).should('be.checked') 

        cy.screenshot('Cadastro preenchido'+dateN+'')

        //cy.get('.submit').click();
        
        function gera_random(n) {
		    var ranNum = Math.round(Math.random()*n);
		    return ranNum;
		}
		function mod(dividendo,divisor) {
			return Math.round(dividendo - (Math.floor(dividendo/divisor)*divisor));
		}
		function cpf() {
			var n = 9;
			var n1 = gera_random(n);
		 	var n2 = gera_random(n);
		 	var n3 = gera_random(n);
		 	var n4 = gera_random(n);
		 	var n5 = gera_random(n);
		 	var n6 = gera_random(n);
		 	var n7 = gera_random(n);
		 	var n8 = gera_random(n);
		 	var n9 = gera_random(n);
		 	var d1 = n9*2+n8*3+n7*4+n6*5+n5*6+n4*7+n3*8+n2*9+n1*10;
		 	d1 = 11 - (mod(d1,11));
		 	if (d1>=10) d1 = 0;
		 	var d2 = d1*2+n9*3+n8*4+n7*5+n6*6+n5*7+n4*8+n3*9+n2*10+n1*11;
		 	d2 = 11 - (mod(d2,11));
		 	if (d2>=10) d2 = 0;
	
			  return ''+n1+n2+n3+n4+n5+n6+n7+n8+n9+d1+d2;
		}
    })
})